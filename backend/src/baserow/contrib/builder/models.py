from baserow.core.models import Application

__all__ = ["Builder"]


class Builder(Application):
    pass
